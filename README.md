# TomatoTodo

ポモドーロ・テクニックの使用者のためのTODO管理アプリです

## 必要となるツール

| ツール     | バージョン |
|------------|-------|
| PostgreSQL | 9.6   |
| Node.js    | 9.3.0 |
| ruby       | 2.4.3 |

環境変数の設定方法にはdirenvを推奨します

## DBの初期化

  `bundle exec rails db:setup`

## DBのリセット方法

`bundle exec rails app:dev:reset`

## テストの走らせ方

`bundle exec rspec`

## アプリの起動方法

1. direnvとrubyとpostgresqlとnodejsとyarnをインストール
1. .envrcに環境変数を追記
1. `$ bundle exec rails db:setup`
1. `$ bundle`
1. `$ yarn`
1. `$ bundle exec foreman start -f Procfile.dev`

[ステージングURL](https://tomato-todo-staging.herokuapp.com/ "ステージングURL")
