require 'securerandom'

include FactoryBot::Syntax::Methods unless Rails.env.production?

namespace :app do
  namespace :dev do
    task reset: %i( db:migrate:reset db:seed app:dev:sample )

    task sample: :environment do
      # ユーザー
      create_list(:user, 2).each do |user|
        create_list(:big_task, 4, user: user)
        create_list(:todays_task, 4, user: user, status: :finished)
        todays_tasks = create_list(:todays_task, 4, user: user)
        todays_tasks.each do |todays_task|
          todays_task.add_tag(SecureRandom.hex(6))
        end

        create(:timer_log,
               user: user,
               started_at: 26.minute.before,
               ended_at: 1.minute.before,
               todays_task: todays_tasks.last)
      end
    end
  end
end
