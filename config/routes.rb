Rails.application.routes.draw do
  resources :todays_tasks, except: %i[edit new] do
    scope module: :todays_tasks do
      resources :tags, only: %i[create destroy]
      resources :timer_logs, only: %i[create]
    end
  end
  resources :big_tasks, except: %i[edit new]
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
  root to: 'welcomes#index'
  get '/statistics', to: 'welcomes#index'

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end
end
