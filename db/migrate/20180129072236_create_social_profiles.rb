class CreateSocialProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :social_profiles do |t|
      t.integer :provider, null: false
      t.string :uid, null: false
      t.string :username, null: false, default: ''
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
