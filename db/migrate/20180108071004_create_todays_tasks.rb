class CreateTodaysTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :todays_tasks do |t|
      t.references :user, foreign_key: true, null: false
      t.integer :expected_pomodoro_count
      t.integer :pomodoro_count
      t.integer :status, null: false, default: 1
      t.integer :priority, default: 1
      t.string :title, null: false
      t.text :memo, null: false, default: ''

      t.timestamps
    end
  end
end
