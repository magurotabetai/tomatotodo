class CreateTimerLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :timer_logs do |t|
      t.datetime :started_at, null: false
      t.datetime :ended_at, null: false
      t.integer :status, null: false, default: 1
      t.references :user, null: false
      t.references :todays_task, null: false

      t.timestamps
    end
  end
end
