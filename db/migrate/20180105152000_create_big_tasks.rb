class CreateBigTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :big_tasks do |t|
      t.string :title, null: false
      t.references :user, foreign_key: true, null: false
      t.integer :status, null: false, default: 1

      t.timestamps
    end
  end
end
