class CreateTodaysTaskTags < ActiveRecord::Migration[5.2]
  def change
    create_table :todays_task_tags do |t|
      t.string :name, null: false
      t.references :user, null: false

      t.timestamps
    end

    add_index :todays_task_tags, %i[name user_id], unique: true
  end
end
