class CreateTodaysTaskTagStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :todays_task_tag_statuses do |t|
      t.references :todays_task_tag, foreign_key: true
      t.references :todays_task, foreign_key: true

      t.timestamps
    end

    add_index :todays_task_tag_statuses, %i[todays_task_tag_id todays_task_id], name: 'unique_todays_task_status', unique: true
  end
end
