# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_02_15_060511) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "big_tasks", force: :cascade do |t|
    t.string "title", null: false
    t.bigint "user_id", null: false
    t.integer "status", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_big_tasks_on_user_id"
  end

  create_table "social_profiles", force: :cascade do |t|
    t.integer "provider", null: false
    t.string "uid", null: false
    t.string "username", default: "", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_social_profiles_on_user_id"
  end

  create_table "timer_logs", force: :cascade do |t|
    t.datetime "started_at", null: false
    t.datetime "ended_at", null: false
    t.integer "status", default: 1, null: false
    t.bigint "user_id", null: false
    t.bigint "todays_task_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["todays_task_id"], name: "index_timer_logs_on_todays_task_id"
    t.index ["user_id"], name: "index_timer_logs_on_user_id"
  end

  create_table "todays_task_tag_statuses", force: :cascade do |t|
    t.bigint "todays_task_tag_id"
    t.bigint "todays_task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["todays_task_id"], name: "index_todays_task_tag_statuses_on_todays_task_id"
    t.index ["todays_task_tag_id", "todays_task_id"], name: "unique_todays_task_status", unique: true
    t.index ["todays_task_tag_id"], name: "index_todays_task_tag_statuses_on_todays_task_tag_id"
  end

  create_table "todays_task_tags", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "user_id"], name: "index_todays_task_tags_on_name_and_user_id", unique: true
    t.index ["user_id"], name: "index_todays_task_tags_on_user_id"
  end

  create_table "todays_tasks", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "expected_pomodoro_count"
    t.integer "pomodoro_count"
    t.integer "status", default: 1, null: false
    t.integer "priority", default: 1
    t.string "title", null: false
    t.text "memo", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_todays_tasks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "big_tasks", "users"
  add_foreign_key "social_profiles", "users"
  add_foreign_key "todays_task_tag_statuses", "todays_task_tags"
  add_foreign_key "todays_task_tag_statuses", "todays_tasks"
  add_foreign_key "todays_tasks", "users"
end
