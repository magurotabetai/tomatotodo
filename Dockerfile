FROM ruby:2.4.3-alpine3.7

RUN apk add --update --no-cache \
  ruby \
  nodejs \
  postgresql-client \
  postgresql-dev \
  ca-certificates \
  ruby-dev \
  build-base \
  bash \
  linux-headers \
  zlib-dev \
  libxml2-dev \
  libxslt-dev \
  tzdata \
  git \
  yarn \
  && rm -rf /var/cache/apk/*

RUN mkdir -p /app
WORKDIR /app

ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock

RUN gem update --system

COPY . /app

RUN gem install bundler io-console --no-ri --no-rdoc && bundle install --jobs 10 --retry 5
