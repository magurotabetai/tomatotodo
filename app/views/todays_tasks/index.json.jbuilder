json.array! @todays_tasks do |todays_task|
  json.id todays_task.id
  json.title todays_task.title
  json.tags do
    json.array! todays_task.todays_task_tags, :id, :name
  end
end
