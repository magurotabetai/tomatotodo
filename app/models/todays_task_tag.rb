# == Schema Information
#
# Table name: todays_task_tags
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_todays_task_tags_on_name_and_user_id  (name,user_id) UNIQUE
#  index_todays_task_tags_on_user_id           (user_id)
#

class TodaysTaskTag < ApplicationRecord
  has_many :todays_task_tag_statuses, dependent: :destroy
  has_many :todays_tasks, through: :todays_task_tag_statuses
  belongs_to :user

  validates :name,
            presence: true,
            length: { maximum: 14 }
end
