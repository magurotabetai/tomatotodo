# == Schema Information
#
# Table name: big_tasks
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  user_id    :integer          not null
#  status     :integer          default("backlog"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_big_tasks_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class BigTask < ApplicationRecord
  belongs_to :user

  enum status: {
    backlog: 1,
    done: 2
  }

  validates :title,
            presence: true,
            length: { maximum: 50 }
end
