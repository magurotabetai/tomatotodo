# == Schema Information
#
# Table name: todays_task_tag_statuses
#
#  id                 :integer          not null, primary key
#  todays_task_tag_id :integer
#  todays_task_id     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_todays_task_tag_statuses_on_todays_task_id      (todays_task_id)
#  index_todays_task_tag_statuses_on_todays_task_tag_id  (todays_task_tag_id)
#  unique_todays_task_status                             (todays_task_tag_id,todays_task_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (todays_task_id => todays_tasks.id)
#  fk_rails_...  (todays_task_tag_id => todays_task_tags.id)
#

class TodaysTaskTagStatus < ApplicationRecord
  belongs_to :todays_task_tag
  belongs_to :todays_task
end
