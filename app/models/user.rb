# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  username           :string           default(""), not null
#  encrypted_password :string           default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :inet
#  last_sign_in_ip    :inet
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

class User < ApplicationRecord
  has_many :big_tasks, dependent: :destroy
  has_many :todays_tasks, dependent: :destroy
  has_many :social_profiles, dependent: :destroy
  has_many :todays_task_tags, dependent: :destroy
  has_many :timer_logs, dependent: :destroy

  validates :username, uniqueness: { case_sensitive: false }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable,
  #        :confirmable, :lockable, :timeoutable
  devise :trackable, :omniauthable, :registerable, :database_authenticatable,
         authentication_keys: [:username], omniauth_providers: %i[twitter]

  def self.from_omniauth(auth)
    username = auth['info']['nickname']
    uid      = auth['uid']
    provider = auth['provider']

    social_profile = SocialProfile.find_by(provider: provider, uid: uid)

    return social_profile.user if social_profile

    user = create(username: username, name: username)

    user.update(username: SecureRandom.hex(10)) until user.persisted?

    user.social_profiles.create!(
      provider: provider,
      uid: uid,
      username: username
    )

    user
  end

  def self.new_with_session(params, session)
    if session['devise.user_attributes']
      new(session['devise.user_attributes']) do |user|
        user.attributes = params
      end
    else
      super
    end
  end
end
