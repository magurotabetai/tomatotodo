# == Schema Information
#
# Table name: todays_tasks
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          not null
#  expected_pomodoro_count :integer
#  pomodoro_count          :integer
#  status                  :integer          default("backlog"), not null
#  priority                :integer          default(1)
#  title                   :string           not null
#  memo                    :text             default(""), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_todays_tasks_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class TodaysTask < ApplicationRecord
  has_many :todays_task_tag_statuses, dependent: :destroy
  has_many :todays_task_tags, through: :todays_task_tag_statuses
  has_many :timer_logs, dependent: :destroy
  belongs_to :user

  enum status: {
    backlog: 1,
    finished: 2
  }

  validates :title,
            presence: true,
            length: { maximum: 50 }

  # add_tag : String -> TodaysTaskTag
  def add_tag(name)
    tag = user.todays_task_tags.find_or_initialize_by(name: name)

    if tag.persisted?
      if todays_task_tags.exists?(tag.id)
        tag.errors.add(:name, 'が重複しています')
        return tag
      end
    end

    todays_task_tags << tag if tag.persisted? || tag.save

    tag
  end

  # remove_tag : String -> TodaysTaskTag
  def remove_tag(name)
    tag = user.todays_task_tags.find_by!(name: name)

    todays_task_tags.destroy(tag.id)
    tag
  end

  # to_json_with_tags : String
  def to_json_with_tags
    to_json(
      only: %i[id title priority],
      include: { todays_task_tags: { only: %i[id name] } }
    )
  end
end
