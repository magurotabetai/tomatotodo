class BigTasksController < ApplicationController
  respond_to :json

  # GET /big_tasks.json
  def index
    @big_tasks = current_user.big_tasks
  end

  # GET /big_tasks/1.json
  def show
    @big_task = find_big_task
  end

  # POST /big_tasks.json
  def create
    @big_task = current_user.big_tasks.new(big_task_params)

    if @big_task.save
      render :show
    else
      render json: @big_task.errors.full_messages, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /big_tasks/1.json
  def update
    @big_task = find_big_task

    if @big_task.update(big_task_params)
      render :show, status: :ok, location: @big_task
    else
      render json: @big_task.errors.full_messages, status: :unprocessable_entity
    end
  end

  # DELETE /big_tasks/1.json
  def destroy
    @big_task = find_big_task

    @big_task.destroy
    head :no_content
  end

  private

  def find_big_task
    current_user.big_tasks.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def big_task_params
    params.require(:big_task).permit(:title, :status)
  end
end
