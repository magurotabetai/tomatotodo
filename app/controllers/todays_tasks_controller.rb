class TodaysTasksController < ApplicationController
  respond_to :json

  # GET /todays_tasks.json
  def index
    @todays_tasks = current_user.todays_tasks.backlog
  end

  # GET /todays_tasks/1.json
  def show
    @todays_task = find_todays_task
  end

  # POST /todays_tasks.json
  def create
    @todays_task = current_user.todays_tasks.new(todays_task_params)

    if @todays_task.save
      render json: @todays_task.to_json_with_tags, status: :created, location: @todays_task
    else
      render json: @todays_task.errors.full_messages, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /todays_tasks/1.json
  def update
    @todays_task = find_todays_task

    if @todays_task.update(todays_task_params)
      render json: @todays_task.to_json_with_tags, status: :ok, location: @todays_task
    else
      render json: @todays_task.errors.full_messages, status: :unprocessable_entity
    end
  end

  # DELETE /todays_tasks/1
  # DELETE /todays_tasks/1.json
  def destroy
    @todays_task = find_todays_task

    @todays_task.destroy
    head :no_content
  end

  private

  def find_todays_task
    current_user.todays_tasks.find(params[:id])
  end

  def todays_task_params
    params.require(:todays_task).permit(
      :user_id, :expected_pomodoro_count, :pomodoro_count, :status, :priority,
      :title, :memo
    )
  end
end
