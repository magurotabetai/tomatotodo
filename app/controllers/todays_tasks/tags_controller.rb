class TodaysTasks::TagsController < ApplicationController
  respond_to :json

  # POST /todays_tasks/1/tags.json
  def create
    todays_task = find_todays_task
    tag = todays_task.add_tag(params[:name])

    if tag.errors.any?
      render json: tag.errors.full_messages, status: :unprocessable_entity
    else
      render json: tag.to_json(only: %i[id name])
    end
  end

  # DELETE /todays_tasks/1/tags/1.json
  def destroy
    todays_task = find_todays_task
    todays_task.todays_task_tags.destroy(params[:id])

    head :no_content
  end

  private

  def find_todays_task
    current_user.todays_tasks.find(params[:todays_task_id])
  end
end
