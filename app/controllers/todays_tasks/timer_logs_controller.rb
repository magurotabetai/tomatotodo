class TodaysTasks::TimerLogsController < ApplicationController
  # POST /todays_tasks/1/timer_logs.json
  def create
    todays_task = current_user.todays_tasks.find(params[:todays_task_id])
    timer_log = todays_task.timer_logs.create(timer_log_params)

    render json: timer_log.to_json, status: :created
  end

  private

  def timer_log_params
    params.permit(
      :status
    ).merge(
      ended_at: Time.current,
      started_at: params[:elapsed_seconds].to_i.seconds.before,
      user: current_user
    )
  end
end
