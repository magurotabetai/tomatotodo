class WelcomesController < ApplicationController
  def index
    big_tasks = current_user.big_tasks.backlog.order(:created_at)
    todays_tasks = current_user
                   .todays_tasks
                   .includes(:todays_task_tags)
                   .backlog
                   .order(:created_at)

    @todays_tasks_json = todays_tasks.to_json(
      only: %i[id title priority],
      include: { todays_task_tags: { only: %i[id name] } }
    )
    @big_tasks_json = big_tasks.to_json(only: %i[id title])

    @statistics_json = {
      todays_tasks_count: current_user.todays_tasks.finished.size,
      big_tasks_count: current_user.big_tasks.done.size,
      pomodoro_count: current_user.timer_logs.done.size
    }.to_json
  end
end
