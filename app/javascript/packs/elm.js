import Main from '../elm/Main'

document.addEventListener('DOMContentLoaded', () => {
  const mainNode = document.getElementById('elm-main')
  const todaysTasks = JSON.parse(mainNode.dataset.todaysTasks)
  const bigTasks = JSON.parse(mainNode.dataset.bigTasks)
  const statistics = JSON.parse(mainNode.dataset.statistics)
  const flags = {
    'todaysTasks': todaysTasks,
    'bigTasks': bigTasks,
    'statistics': statistics
  }
  const main = Main.Main.embed(mainNode, flags)

  main.ports.notify.subscribe(notifyMessage => {
    new Notification(notifyMessage)
  })

  main.ports.changeTitle.subscribe(title => {
    document.title = title + 'TomatoTodo'
  })
})
