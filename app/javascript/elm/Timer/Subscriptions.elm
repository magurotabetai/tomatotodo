module Timer.Subscriptions exposing (subscriptions)

import Time exposing (second)
import Timer.Model exposing (Timer)
import Timer.Msg exposing (TimerMsg(..))


subscriptions : Timer -> Sub TimerMsg
subscriptions timer =
    if timer.paused then
        Sub.none
    else
        Time.every second Tick
