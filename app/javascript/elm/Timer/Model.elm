module Timer.Model exposing (Timer, Mode(..), initModel, humanizeTime)


type alias Timer =
    { mode : Mode
    , paused : Bool
    , count : Int
    }


type Mode
    = Pomodoro
    | Short
    | Long


initModel : Timer
initModel =
    Timer Pomodoro True 1500


prependZero : Int -> String
prependZero n =
    if n < 10 then
        "0" ++ toString n
    else
        toString n


humanizeTime : Int -> String
humanizeTime t =
    let
        hours =
            t // 3600

        minutes =
            (t - hours) // 60

        seconds =
            t - hours * 3600 - minutes * 60
    in
        prependZero minutes ++ ":" ++ prependZero seconds
