module Timer.View exposing (view)

import Html exposing (Html, h1, text, div, button, span)
import Html.Attributes exposing (class, classList, disabled)
import Html.Events exposing (onClick)
import Timer.Model exposing (Timer, Mode(..), humanizeTime)
import Timer.Msg exposing (TimerMsg(..))
import Msg exposing (Msg(..))


view : Timer -> Bool -> Html Msg
view model doing =
    div [ class "card" ]
        [ div [ class "card-content" ]
            [ div [ class "buttons has-addons is-centered" ]
                [ switcher "Pomodoro" model.mode Pomodoro
                , switcher "Short Break" model.mode Short
                , switcher "Long Break" model.mode Long
                ]
            , h1 [ class "has-text-centered title is-1" ]
                [ text (model.count |> humanizeTime) ]
            , div [ class "buttons is-centered" ]
                [ toggleButton model doing
                , button [ class "button is-danger", onClick (TimerMsg Reset) ]
                    [ text "RESET" ]
                ]
            , if doing then
                text ""
              else
                div [ class "notification is-warning" ]
                    [ text "タイマーを開始するにはタスクを1つ進行中にしてください" ]
            ]
        ]


toggleButton : Timer -> Bool -> Html Msg
toggleButton timer doing =
    let
        ( btnClass, value ) =
            if timer.paused then
                ( "is-primary", "START" )
            else
                ( "is-warning", "PAUSE" )
    in
        if (timer.count == 0 && timer.paused) || not doing then
            button [ class ("button " ++ btnClass), disabled True ]
                [ text value ]
        else
            button [ class ("button " ++ btnClass), onClick (TimerMsg Toggle) ]
                [ text value ]


switcher : String -> Mode -> Mode -> Html Msg
switcher value currentMode mode =
    span
        [ onClick (TimerMsg (SwitchTo mode))
        , classList
            [ ( "button", True )
            , ( "is-selected is-primary", currentMode == mode )
            ]
        ]
        [ text value ]
