port module Timer.Update exposing (update, resetTime)

import Json.Encode
import HttpBuilder exposing (withJsonBody, send)
import Timer.Model exposing (Timer, Mode(..), humanizeTime)
import Timer.Msg exposing (TimerMsg(..), Status(..))
import TodaysTasks.TodaysTask.Model exposing (TodaysTaskId)
import Helper


port notify : String -> Cmd msg


port changeTitle : String -> Cmd msg


type alias TimerLog =
    { count : String
    , status : String
    }


update : TimerMsg -> Timer -> TodaysTaskId -> ( Timer, Cmd TimerMsg )
update msg timer doingTaskId =
    case msg of
        NoOp ->
            timer ! []

        SwitchTo newMode ->
            { timer | mode = newMode, count = resetTime newMode, paused = True } ! [ changeTitle "" ]

        Toggle ->
            let
                updatedTimer =
                    { timer | paused = not timer.paused }
            in
                if timer.paused then
                    updatedTimer ! []
                else
                    updatedTimer ! []

        Tick _ ->
            if timer.count <= 0 then
                if timer.mode == Pomodoro then
                    { timer | paused = True }
                        ! [ notify "タイマーが終了しました", createTimerLogRequest (resetTime timer.mode) "done" doingTaskId ]
                else
                    { timer | paused = True } ! [ notify "タイマーが終了しました" ]
            else
                { timer | count = timer.count - 1 } ! [ changeTitle ("(" ++ humanizeTime (timer.count - 1) ++ ") ") ]

        Reset ->
            let
                updatedTimer =
                    { timer | count = resetTime timer.mode, paused = True }

                elapsedSeconds =
                    resetTime timer.mode - timer.count
            in
                if timer.mode == Pomodoro && timer.count /= 0 then
                    updatedTimer ! [ createTimerLogRequest elapsedSeconds "stopped" doingTaskId, changeTitle "" ]
                else
                    updatedTimer ! [ changeTitle "" ]

        CreateTimerLog _ ->
            timer ! []


resetTime : Mode -> Int
resetTime mode =
    case mode of
        Pomodoro ->
            1500

        Short ->
            300

        Long ->
            600


createTimerLogRequest : Int -> String -> TodaysTaskId -> Cmd TimerMsg
createTimerLogRequest count status todaysTaskId =
    HttpBuilder.post ("/todays_tasks/" ++ toString todaysTaskId ++ "/timer_logs.json")
        |> withJsonBody (encodeTimerLog (TimerLog (toString count) status))
        |> Helper.withRailsDefault
        |> send CreateTimerLog


encodeTimerLog : TimerLog -> Json.Encode.Value
encodeTimerLog record =
    Json.Encode.object
        [ ( "elapsed_seconds", Json.Encode.string <| record.count )
        , ( "status", Json.Encode.string <| record.status )
        ]
