module Timer.Msg exposing (TimerMsg(..), Status(..))

import Http
import Timer.Model exposing (Mode)
import Time exposing (Time)


type Status
    = Done
    | Stopped


type TimerMsg
    = NoOp
    | SwitchTo Mode
    | Toggle
    | Tick Time
    | Reset
    | CreateTimerLog (Result Http.Error ())
