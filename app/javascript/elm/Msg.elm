module Msg exposing (Msg(..))

import Navigation
import Timer.Msg as Timer
import TodaysTasks.Msg as TodaysTasks
import BigTask.Msg as BigTask


type Msg
    = NoOp
    | TimerMsg Timer.TimerMsg
    | TodaysTasksMsg TodaysTasks.TodaysTasksMsg
    | BigTaskMsg BigTask.BigTaskMsg
    | NewUrl String
    | UrlChange Navigation.Location
