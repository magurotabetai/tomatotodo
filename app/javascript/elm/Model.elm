module Model exposing (Model, initModel, Flags, route, Route(..))

import UrlParser as Url exposing (Parser)
import Timer.Model exposing (Timer)
import TodaysTasks.Model exposing (TodaysTasks, TodaysTasksFlag)
import BigTask.Model exposing (BigTasksFlag)
import Statistics.Model exposing (Statistics)


type Route
    = Index
    | Statistics


route : Parser (Route -> a) a
route =
    Url.oneOf
        [ Url.map Index Url.top
        , Url.map Statistics (Url.s "statistics")
        ]


type alias Model =
    { timer : Timer
    , todaysTasks : TodaysTasks
    , bigTasks : BigTask.Model.Model
    , page : Maybe Route
    , statistics : Statistics
    }


type alias Flags =
    { todaysTasks : TodaysTasksFlag
    , bigTasks : BigTasksFlag
    , statistics : Statistics
    }


initModel : Flags -> Model
initModel flags =
    Model Timer.Model.initModel
        (TodaysTasks.Model.initTodaysTasks flags.todaysTasks)
        (BigTask.Model.initBigTasks flags.bigTasks)
        Nothing
        flags.statistics
