module Update exposing (update)

import Navigation
import UrlParser as Url
import Msg exposing (Msg(..))
import Model exposing (Model, route)
import Timer.Update as Timer
import TodaysTasks.Update as TodaysTasks
import BigTask.Update as BigTask
import Tuple


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            model ! []

        TimerMsg timerMsg ->
            let
                doingTaskId =
                    Maybe.withDefault 0 model.todaysTasks.doingTaskId

                timer =
                    Timer.update timerMsg model.timer doingTaskId
            in
                { model | timer = Tuple.first timer }
                    ! [ Cmd.map TimerMsg (Tuple.second timer) ]

        TodaysTasksMsg todaysTasksMsg ->
            let
                todaysTasks =
                    TodaysTasks.update todaysTasksMsg model.todaysTasks
            in
                { model | todaysTasks = Tuple.first todaysTasks }
                    ! [ Cmd.map TodaysTasksMsg (Tuple.second todaysTasks) ]

        BigTaskMsg bigTaskMsg ->
            let
                bigTasks =
                    BigTask.update bigTaskMsg model.bigTasks
            in
                { model | bigTasks = Tuple.first bigTasks }
                    ! [ Cmd.map BigTaskMsg (Tuple.second bigTasks) ]

        NewUrl url ->
            model ! [ Navigation.newUrl url ]

        UrlChange location ->
            { model | page = Url.parsePath route location } ! []
