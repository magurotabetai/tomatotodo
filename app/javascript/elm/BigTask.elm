module BigTask exposing (main)

import Html
import BigTask.Model exposing (BigTask, Model)
import BigTask.Update exposing (Msg, update)
import BigTask.View exposing (view)


type alias Flags =
    List BigTask


init : List BigTask -> ( Model, Cmd Msg )
init flags =
    ( Model flags "" [], Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


main : Program Flags Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
