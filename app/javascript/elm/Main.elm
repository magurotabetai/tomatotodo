module Main exposing (main)

import Navigation
import UrlParser as Url
import Model exposing (Model, initModel, Flags, route)
import View exposing (view)
import Msg exposing (Msg(..))
import Update exposing (update)
import Timer.Subscriptions


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags location =
    let
        model =
            initModel flags
    in
        { model | page = Url.parsePath route location } ! []


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.timer.paused then
        Sub.none
    else
        Sub.map TimerMsg (Timer.Subscriptions.subscriptions model.timer)


main : Program Flags Model Msg
main =
    Navigation.programWithFlags UrlChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
