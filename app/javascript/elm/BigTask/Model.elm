module BigTask.Model exposing (Model, BigTask, BigTaskId, TaskTitle, newBigTask, BigTasksFlag, initBigTasks, InputStatus(..))


type alias BigTaskId =
    Int


type alias TaskTitle =
    String


type InputStatus
    = None
    | Editing
    | Sending


type alias BigTask =
    { id : BigTaskId
    , title : TaskTitle
    , editing : Bool
    , input : TaskTitle
    , errors : List String
    }


type alias BigTaskFlag =
    { id : BigTaskId
    , title : TaskTitle
    }


type alias BigTasksFlag =
    List BigTaskFlag


type alias Model =
    { bigTasks : List BigTask
    , input : TaskTitle
    , errors : List String
    , inputStatus : InputStatus
    }


newBigTask : BigTaskId -> TaskTitle -> BigTask
newBigTask bigTaskId title =
    { id = bigTaskId
    , title = title
    , editing = False
    , input = title
    , errors = []
    }


initBigTasks : BigTasksFlag -> Model
initBigTasks flags =
    let
        flagToBigTasks flag =
            newBigTask flag.id flag.title
    in
        Model (List.map flagToBigTasks flags) "" [] None
