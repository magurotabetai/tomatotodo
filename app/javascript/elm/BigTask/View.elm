module BigTask.View exposing (view)

import Html exposing (Html, p, text, div, input, button, header)
import Html.Attributes exposing (class, type_, value, classList, id, disabled)
import Html.Events exposing (on, onClick, onInput, keyCode, onDoubleClick, onBlur)
import Json.Decode as Json
import BigTask.Model exposing (Model, BigTask, TaskTitle, InputStatus(..))
import BigTask.Msg exposing (BigTaskMsg(..))
import Helper exposing (errorsView)
import Msg exposing (Msg)


onEnter : BigTaskMsg -> Html.Attribute BigTaskMsg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Json.succeed msg
            else
                Json.fail "not ENTER"
    in
        on "keypress" (Json.andThen isEnter keyCode)


view : Model -> Html Msg
view model =
    Html.map Msg.BigTaskMsg (bigTasksView model)


bigTasksView : Model -> Html BigTaskMsg
bigTasksView model =
    div [ class "card" ]
        [ header [ class "card-header-title" ]
            [ text "アクティビティ在庫一覧" ]
        , div [ class "card-content p-xs" ]
            (List.map bigTaskView model.bigTasks ++ [ form model.input model.errors model.inputStatus ])
        ]


bigTaskView : BigTask -> Html BigTaskMsg
bigTaskView bigTask =
    let
        editForm =
            div [ class "field" ]
                ([ input
                    [ value bigTask.input
                    , classList
                        [ ( "input", True )
                        , ( "is-danger", not (List.isEmpty bigTask.errors) )
                        ]
                    , id ("bigTask-" ++ toString bigTask.id)
                    , type_ "text"
                    , onEnter (EditComplete bigTask)
                    , onInput (UpdateEditingText bigTask)
                    , onBlur (UnEditingEntry bigTask)
                    ]
                    []
                 ]
                    ++ errorsView bigTask.errors
                )

        taskContent =
            div []
                [ p [ class "card-text" ]
                    [ text bigTask.title ]
                , div [ class "buttons" ]
                    [ button [ class "button is-warning is-small", onClick (EditingEntry bigTask) ]
                        [ text "編集" ]
                    , button [ class "button is-primary is-small", onClick (CompleteTask bigTask.id) ]
                        [ text "完了" ]
                    , button [ class "button is-danger is-small", onClick (Delete bigTask.id) ]
                        [ text "削除" ]
                    ]
                ]
    in
        div [ class "card m-r-xs m-l-xs m-b-sm", onDoubleClick (EditingEntry bigTask) ]
            [ div [ class "card-content p-md big-task-card" ]
                [ if bigTask.editing then
                    editForm
                  else
                    taskContent
                ]
            ]


form : TaskTitle -> List String -> InputStatus -> Html BigTaskMsg
form newTodo errors inputStatus =
    div [ class "card m-r-xs m-l-xs m-b-xs" ]
        [ div [ class "card-content" ]
            [ div [ class "field" ]
                ([ input
                    [ type_ "text"
                    , value newTodo
                    , onInput Input
                    , onEnter Submit
                    , classList
                        [ ( "input", True )
                        , ( "is-danger", not (List.isEmpty errors) )
                        ]
                    ]
                    []
                 ]
                    ++ errorsView errors
                )
            , if inputStatus == Sending then
                button [ class "button is-primary is-loading", disabled True ]
                    [ text "追加" ]
              else
                button [ class "button is-primary", onClick Submit ]
                    [ text "追加" ]
            ]
        ]
