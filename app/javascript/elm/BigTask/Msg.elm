module BigTask.Msg exposing (BigTaskMsg(..))

import Http
import BigTask.Model exposing (BigTask, BigTaskId, TaskTitle)


type BigTaskMsg
    = NoOp
    | Add BigTaskId TaskTitle
    | Input String
    | Delete BigTaskId
    | CreateTask (Result Http.Error BigTask)
    | DestroyTask BigTaskId (Result Http.Error ())
    | UpdateTask BigTask (Result Http.Error ())
    | EditingEntry BigTask
    | EditComplete BigTask
    | Submit
    | UpdateEditingText BigTask String
    | UnEditingEntry BigTask
    | CompleteTask BigTaskId
