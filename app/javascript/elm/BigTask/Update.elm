module BigTask.Update exposing (update)

import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline exposing (required, decode)
import HttpBuilder exposing (withExpect, withJsonBody, send)
import Dom
import Task
import BigTask.Model exposing (Model, BigTask, BigTaskId, TaskTitle, newBigTask, InputStatus(..))
import BigTask.Msg exposing (BigTaskMsg(..))
import Helper exposing (decodeError)


type RequestType
    = Create TaskTitle
    | Update BigTask
    | Destroy BigTaskId
    | Complete BigTaskId


update : BigTaskMsg -> Model -> ( Model, Cmd BigTaskMsg )
update msg model =
    case msg of
        NoOp ->
            model ! []

        Add id title ->
            { model | bigTasks = model.bigTasks ++ [ newBigTask id title ], input = "", inputStatus = None } ! []

        Input textbox ->
            { model | input = textbox, errors = [] } ! []

        Submit ->
            { model | inputStatus = Sending } ! [ request (Create model.input) ]

        CreateTask (Ok bigTask) ->
            update (Add bigTask.id bigTask.title) model

        CreateTask (Err error) ->
            case error of
                Http.BadStatus response ->
                    { model | errors = decodeError response.body, inputStatus = None } ! []

                _ ->
                    { model | inputStatus = None } ! []

        Delete bigTaskId ->
            model ! [ request (Destroy bigTaskId) ]

        DestroyTask bigTaskId (Ok _) ->
            { model | bigTasks = removeBigTask model.bigTasks bigTaskId } ! []

        DestroyTask _ (Err _) ->
            model ! []

        UpdateTask bigTask (Ok _) ->
            updateBigTasks model { bigTask | editing = False, title = bigTask.input } ! []

        UpdateTask bigTask (Err error) ->
            case error of
                Http.BadStatus response ->
                    updateBigTasks model { bigTask | errors = decodeError response.body } ! []

                _ ->
                    model ! []

        EditingEntry bigTask ->
            let
                focus =
                    Dom.focus ("bigTask-" ++ toString bigTask.id)
            in
                updateBigTasks model { bigTask | editing = True }
                    ! [ Task.attempt (\_ -> NoOp) focus ]

        EditComplete bigTask ->
            model ! [ request (Update bigTask) ]

        UpdateEditingText bigTask textBox ->
            updateBigTasks model { bigTask | input = textBox, errors = [] } ! []

        UnEditingEntry bigTask ->
            updateBigTasks model { bigTask | editing = False } ! []

        CompleteTask bigTaskId ->
            model ! [ request (Complete bigTaskId) ]


updateBigTasks : Model -> BigTask -> Model
updateBigTasks model updatedBigTask =
    let
        updateBigTask currentBigTask =
            if updatedBigTask.id == currentBigTask.id then
                updatedBigTask
            else
                currentBigTask
    in
        { model | bigTasks = List.map updateBigTask model.bigTasks }


removeBigTask : List BigTask -> BigTaskId -> List BigTask
removeBigTask bigTasks targetId =
    let
        choose id bigTask =
            bigTask.id /= id
    in
        List.filter (choose targetId) bigTasks


request : RequestType -> Cmd BigTaskMsg
request requestType =
    case requestType of
        Create taskTitle ->
            HttpBuilder.post "/big_tasks.json"
                |> withJsonBody (bigTaskEncoder (Create taskTitle))
                |> withExpect (Http.expectJson bigTaskResult)
                |> Helper.withRailsDefault
                |> send CreateTask

        Update bigTask ->
            HttpBuilder.put ("/big_tasks/" ++ toString bigTask.id ++ ".json")
                |> withJsonBody (bigTaskEncoder (Update bigTask))
                |> Helper.withRailsDefault
                |> send (UpdateTask bigTask)

        Destroy bigTaskId ->
            HttpBuilder.delete ("/big_tasks/" ++ toString bigTaskId ++ ".json")
                |> Helper.withRailsDefault
                |> send (DestroyTask bigTaskId)

        Complete bigTaskId ->
            HttpBuilder.put ("/big_tasks/" ++ toString bigTaskId ++ ".json")
                |> withJsonBody (bigTaskEncoder (Complete bigTaskId))
                |> Helper.withRailsDefault
                |> send (DestroyTask bigTaskId)


bigTaskResult : Decode.Decoder BigTask
bigTaskResult =
    decode newBigTask
        |> required "id" Decode.int
        |> required "title" Decode.string


bigTaskEncoder : RequestType -> Encode.Value
bigTaskEncoder requestType =
    case requestType of
        Create bigTaskTitle ->
            Encode.object
                [ ( "title", Encode.string <| bigTaskTitle ) ]

        Update bigTask ->
            Encode.object
                [ ( "id", Encode.int <| bigTask.id )
                , ( "title", Encode.string <| bigTask.input )
                ]

        Destroy _ ->
            Encode.object []

        Complete _ ->
            Encode.object [ ( "status", Encode.string <| "done" ) ]
