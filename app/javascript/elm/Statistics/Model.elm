module Statistics.Model exposing (Statistics)


type alias Statistics =
    { big_tasks_count : Int
    , todays_tasks_count : Int
    , pomodoro_count : Int
    }
