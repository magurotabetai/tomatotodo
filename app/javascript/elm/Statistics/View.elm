module Statistics.View exposing (view)

import Html exposing (Html, div, text, header, p)
import Html.Attributes exposing (class)
import Msg exposing (Msg)
import Statistics.Model exposing (Statistics)


view : Statistics -> Html Msg
view statistics =
    div [ class "card" ]
        [ header [ class "card-header" ]
            [ p [ class "card-header-title" ]
                [ text "進捗状況" ]
            ]
        , div [ class "card-content" ]
            [ div [ class "content" ]
                [ div [ class "columns" ]
                    [ statisticCard "完了したアクティビティ" (toString statistics.big_tasks_count)
                    , statisticCard "完了したタスク" (toString statistics.todays_tasks_count)
                    , statisticCard "達成したポモドーロ" (toString statistics.pomodoro_count)
                    ]
                ]
            ]
        ]


statisticCard : String -> String -> Html Msg
statisticCard title val =
    div [ class "column" ]
        [ div [ class "card" ]
            [ header [ class "card-header" ]
                [ p [ class "card-header-title" ]
                    [ text title ]
                ]
            , div [ class "card-content" ]
                [ div [ class "content" ]
                    [ text val ]
                ]
            ]
        ]
