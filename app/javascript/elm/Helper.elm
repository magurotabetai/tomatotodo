module Helper exposing (withRailsDefault, onEnter, decodeError, errorsView)

import Rails
import HttpBuilder exposing (RequestBuilder, withCredentials, withHeader)
import Html exposing (Html, p, text)
import Html.Attributes exposing (class)
import Html.Events exposing (on, keyCode)
import Json.Decode as Json


csrfToken : String
csrfToken =
    Result.withDefault "" Rails.csrfToken


withRailsDefault : RequestBuilder a -> RequestBuilder a
withRailsDefault request =
    request
        |> withCredentials
        |> withHeader "X-CSRF-TOKEN" csrfToken


onEnter : msg -> Html.Attribute msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                Json.succeed msg
            else
                Json.fail "not ENTER"
    in
        on "keypress" (Json.andThen isEnter keyCode)


decodeError : String -> List String
decodeError body =
    Json.decodeString (Json.list Json.string) body
        |> Result.withDefault []


errorsView : List String -> List (Html msg)
errorsView errors =
    let
        errorView error =
            p [ class "help is-danger" ]
                [ text error ]
    in
        List.map errorView errors
