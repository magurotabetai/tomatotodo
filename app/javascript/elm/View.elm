module View exposing (view)

import Html exposing (Html, div, nav, h3, text, a)
import Html.Attributes exposing (class, id, attribute, href, classList)
import Html.Events exposing (onClick)
import Timer.View as Timer
import TodaysTasks.View as TodaysTasks
import BigTask.View as BigTask
import Statistics.View as Statistics
import Model exposing (Model, Route(..))
import Msg exposing (Msg(..))


viewRoute : Maybe Route -> Model -> Html Msg
viewRoute maybeRoute model =
    case maybeRoute of
        Nothing ->
            h3 [] [ text "404 Page Not Found!" ]

        Just route ->
            viewPage route model


viewPage : Route -> Model -> Html Msg
viewPage route model =
    case route of
        Index ->
            div [ class "columns" ]
                [ div [ class "column is-one-third" ]
                    [ BigTask.view model.bigTasks ]
                , div [ class "column is-one-third" ]
                    [ TodaysTasks.view model.todaysTasks ]
                , div [ class "column is-one-third" ]
                    [ Timer.view model.timer (model.todaysTasks.doingTaskId /= Nothing) ]
                ]

        Statistics ->
            Statistics.view model.statistics


view : Model -> Html Msg
view model =
    div [ id "app-main" ]
        [ nav [ attribute "aria-label" "main navigation", class "navbar is-light", attribute "role" "navigation" ]
            [ div [ class "navbar-brand" ]
                [ h3 [ class "title is-3" ]
                    [ text "TomatoTodo" ]
                ]
            , div [ class "navbar-menu" ]
                [ div [ class "navbar-start" ]
                    [ a
                        [ classList
                            [ ( "navbar-item", True )
                            , ( "is-active", model.page == Just Index )
                            ]
                        , onClick (NewUrl "/")
                        ]
                        [ text "ホーム" ]
                    , a
                        [ classList
                            [ ( "navbar-item", True )
                            , ( "is-active", model.page == Just Statistics )
                            ]
                        , onClick (NewUrl "statistics")
                        ]
                        [ text "統計" ]
                    ]
                , div [ class "navbar-end" ]
                    [ a [ class "navbar-item", href "/users/sign_out" ]
                        [ text "ログアウト" ]
                    ]
                ]
            ]
        , div [ class "container m-t-md" ]
            [ viewRoute model.page model ]
        ]
