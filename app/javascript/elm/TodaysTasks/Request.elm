module TodaysTasks.Request exposing (todaysTaskRequest, RequestType(..))

import TodaysTasks.TodaysTask.Model exposing (TaskTitle, TodaysTask, TodaysTaskId, newTodaysTask)
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import HttpBuilder exposing (withExpect, withJsonBody, send)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline exposing (decode, required)
import Helper
import TodaysTasks.Tag.Model exposing (Tag)


type RequestType
    = Create TaskTitle
    | Update TodaysTask
    | Destroy TodaysTaskId
    | Complete TodaysTaskId


todaysTaskRequest : RequestType -> Cmd TodaysTasksMsg
todaysTaskRequest requestType =
    case requestType of
        Create taskTitle ->
            HttpBuilder.post "/todays_tasks.json"
                |> withJsonBody (todaysTaskEncoder (Create taskTitle))
                |> withExpect (Http.expectJson decodeTodaysTask)
                |> Helper.withRailsDefault
                |> send CreateTask

        Update todaysTask ->
            HttpBuilder.put ("/todays_tasks/" ++ toString todaysTask.id ++ ".json")
                |> withJsonBody (todaysTaskEncoder (Update todaysTask))
                |> withExpect (Http.expectJson decodeTodaysTask)
                |> Helper.withRailsDefault
                |> send (UpdateTask todaysTask)

        Destroy todaysTaskId ->
            HttpBuilder.delete ("/todays_tasks/" ++ toString todaysTaskId ++ ".json")
                |> Helper.withRailsDefault
                |> send (DestroyTask todaysTaskId)

        Complete todaysTaskId ->
            HttpBuilder.put ("/todays_tasks/" ++ toString todaysTaskId ++ ".json")
                |> withJsonBody (todaysTaskEncoder (Complete todaysTaskId))
                |> Helper.withRailsDefault
                |> send (DestroyTask todaysTaskId)


decodeTodaysTask : Decode.Decoder TodaysTask
decodeTodaysTask =
    decode newTodaysTask
        |> required "id" Decode.int
        |> required "title" Decode.string
        |> required "priority" Decode.int
        |> required "todays_task_tags" (Decode.list decodeTag)


decodeTag : Decode.Decoder Tag
decodeTag =
    decode Tag
        |> required "id" Decode.int
        |> required "name" Decode.string


todaysTaskEncoder : RequestType -> Encode.Value
todaysTaskEncoder requestType =
    case requestType of
        Create todaysTaskTitle ->
            Encode.object
                [ ( "title", Encode.string <| todaysTaskTitle ) ]

        Update todaysTask ->
            Encode.object
                [ ( "id", Encode.int <| todaysTask.id )
                , ( "title", Encode.string <| todaysTask.input )
                , ( "priority", Encode.int <| todaysTask.priority )
                ]

        Destroy _ ->
            Encode.object []

        Complete _ ->
            Encode.object [ ( "status", Encode.string <| "finished" ) ]
