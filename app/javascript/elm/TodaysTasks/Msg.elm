module TodaysTasks.Msg exposing (TodaysTasksMsg(..))

import Http
import TodaysTasks.TodaysTask.Model exposing (TodaysTaskId, TaskTitle, TodaysTask)
import TodaysTasks.Tag.Model exposing (Tag, TagId, TagName)


type TodaysTasksMsg
    = NoOp
    | UpdateNewTextForm TaskTitle
    | UpdateEditTextForm TodaysTask TaskTitle
    | UpdateNewTagTextForm TodaysTask TagName
    | UnEditingEntry TodaysTask
    | EditingEntry TodaysTask
    | CreateTask (Result Http.Error TodaysTask)
    | DestroyTask TodaysTaskId (Result Http.Error ())
    | UpdateTask TodaysTask (Result Http.Error TodaysTask)
    | SendDestroyRequest TodaysTaskId
    | SendCompleteRequest TodaysTaskId
    | SendUpdateRequest TodaysTask
    | SendCreateRequest
    | DestroyTag TodaysTask TagId (Result Http.Error ())
    | CreateTag TodaysTask (Result Http.Error Tag)
    | SendDestroyTagRequest TodaysTask TagId
    | SendCreateTagRequest TodaysTask TagName
    | SetTaskDoing TodaysTaskId
