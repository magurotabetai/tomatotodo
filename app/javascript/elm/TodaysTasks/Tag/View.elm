module TodaysTasks.Tag.View exposing (tagList, tagView, tagCreateForm)

import Html exposing (Html, span, text, button, div, p, input, label)
import Html.Attributes exposing (class, classList, placeholder, type_, value)
import Html.Events exposing (onClick, onInput)
import TodaysTasks.TodaysTask.Model exposing (TodaysTask)
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import Helper exposing (onEnter)
import TodaysTasks.Tag.Model exposing (Tag)


tagView : List Tag -> Bool -> Html TodaysTasksMsg
tagView tags doing =
    div [ class "tags m-b-none" ]
        ([ if doing then
            span [ class "tag is-primary" ]
                [ text "Doing" ]
           else
            text ""
         ]
            ++ List.map
                (\tag ->
                    span [ class "tag is-info" ]
                        [ text tag.name ]
                )
                tags
        )


tagList : TodaysTask -> Html TodaysTasksMsg
tagList todaysTask =
    div [ class "field" ]
        [ label [ class "label" ]
            [ text "タグ一覧" ]
        , div [ class "tags" ]
            (List.map (tagRemoveForm todaysTask) todaysTask.tags)
        ]


tagRemoveForm : TodaysTask -> Tag -> Html TodaysTasksMsg
tagRemoveForm todaysTask tag =
    span [ class "tag is-primary" ]
        [ text tag.name
        , button [ class "delete is-small m-l-xs", onClick (SendDestroyTagRequest todaysTask tag.id) ]
            []
        ]


tagCreateForm : TodaysTask -> Html TodaysTasksMsg
tagCreateForm todaysTask =
    div [ class "field is-grouped" ]
        [ p [ class "control is-expanded" ]
            ([ input
                [ classList
                    [ ( "input", True )
                    , ( "is-danger", not (List.isEmpty todaysTask.tagErrors) )
                    ]
                , placeholder "新しいタグの追加"
                , type_ "text"
                , value todaysTask.tagInput
                , onInput (UpdateNewTagTextForm todaysTask)
                , onEnter (SendCreateTagRequest todaysTask todaysTask.tagInput)
                ]
                []
             ]
                ++ List.map errorView todaysTask.tagErrors
            )
        , p [ class "control" ]
            [ button [ class "button is-primary", onClick (SendCreateTagRequest todaysTask todaysTask.tagInput) ]
                [ text "追加" ]
            ]
        ]


errorView : String -> Html TodaysTasksMsg
errorView error =
    p [ class "help is-danger" ]
        [ text error ]
