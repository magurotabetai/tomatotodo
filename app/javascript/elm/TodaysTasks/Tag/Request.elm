module TodaysTasks.Tag.Request exposing (destroyTagRequest, createTagRequest)

import HttpBuilder exposing (withExpect, withJsonBody, send)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Json.Decode.Pipeline
import Helper
import TodaysTasks.TodaysTask.Model exposing (TodaysTask)
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import TodaysTasks.Tag.Model exposing (TagId, TagName, Tag)


destroyTagRequest : TodaysTask -> TagId -> Cmd TodaysTasksMsg
destroyTagRequest todaysTask tagId =
    let
        url =
            "/todays_tasks/" ++ toString todaysTask.id ++ "/tags/" ++ toString tagId ++ ".json"
    in
        HttpBuilder.delete url
            |> Helper.withRailsDefault
            |> send (DestroyTag todaysTask tagId)


createTagRequest : TodaysTask -> TagName -> Cmd TodaysTasksMsg
createTagRequest todaysTask tagName =
    HttpBuilder.post ("/todays_tasks/" ++ toString todaysTask.id ++ "/tags.json")
        |> withJsonBody (encodeTag tagName)
        |> withExpect (Http.expectJson decodeTag)
        |> Helper.withRailsDefault
        |> send (CreateTag todaysTask)


encodeTag : TagName -> Encode.Value
encodeTag tagName =
    Encode.object
        [ ( "name", Encode.string <| tagName ) ]


decodeTag : Decode.Decoder Tag
decodeTag =
    Json.Decode.Pipeline.decode Tag
        |> Json.Decode.Pipeline.required "id" Decode.int
        |> Json.Decode.Pipeline.required "name" Decode.string
