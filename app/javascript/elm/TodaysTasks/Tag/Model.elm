module TodaysTasks.Tag.Model exposing (Tag, TagId, TagName)


type alias TagId =
    Int


type alias TagName =
    String


type alias Tag =
    { id : TagId
    , name : TagName
    }
