module TodaysTasks.Update exposing (update)

import Http
import Dom
import Task
import Json.Decode as Decode exposing (list, string)
import TodaysTasks.Model exposing (TodaysTasks)
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import TodaysTasks.Request exposing (todaysTaskRequest, RequestType(..))
import TodaysTasks.Tag.Request exposing (destroyTagRequest, createTagRequest)
import TodaysTasks.TodaysTask.Model exposing (TodaysTask, TodaysTaskId, InputStatus(..))
import Helper exposing (decodeError)


update : TodaysTasksMsg -> TodaysTasks -> ( TodaysTasks, Cmd TodaysTasksMsg )
update msg todaysTasks =
    case msg of
        UpdateNewTextForm newText ->
            { todaysTasks | input = newText, errors = [] } ! []

        UpdateEditTextForm todaysTask newText ->
            updateTodaysTasks todaysTasks { todaysTask | input = newText, errors = [] } ! []

        UpdateNewTagTextForm todaysTask newText ->
            updateTodaysTasks todaysTasks { todaysTask | tagInput = newText, tagErrors = [] } ! []

        UnEditingEntry todaysTask ->
            updateTodaysTasks todaysTasks { todaysTask | status = None } ! []

        EditingEntry todaysTask ->
            let
                focus =
                    Dom.focus ("todaysTask-" ++ toString todaysTask.id)
            in
                updateTodaysTasks todaysTasks { todaysTask | status = Editing }
                    ! [ Task.attempt (\_ -> NoOp) focus ]

        SendCreateRequest ->
            { todaysTasks | inputStatus = Sending } ! [ todaysTaskRequest (Create todaysTasks.input) ]

        CreateTask (Ok todaysTask) ->
            { todaysTasks
                | todaysTasks =
                    todaysTasks.todaysTasks ++ [ todaysTask ]
                , input = ""
                , inputStatus = None
            }
                ! []

        CreateTask (Err error) ->
            case error of
                Http.BadStatus response ->
                    let
                        errors =
                            Decode.decodeString (list string) response.body
                                |> Result.withDefault []
                    in
                        { todaysTasks | errors = errors, inputStatus = None } ! []

                _ ->
                    { todaysTasks | inputStatus = None } ! []

        DestroyTask targetId (Ok _) ->
            let
                updatedTodaysTasks =
                    { todaysTasks
                        | todaysTasks =
                            removeTodaysTask todaysTasks.todaysTasks targetId
                    }
            in
                (if List.isEmpty updatedTodaysTasks.todaysTasks then
                    { updatedTodaysTasks | doingTaskId = Nothing }
                 else
                    updatedTodaysTasks
                )
                    ! []

        UpdateTask _ (Ok todaysTask) ->
            updateTodaysTasks todaysTasks { todaysTask | status = Editing } ! []

        UpdateTask todaysTask (Err error) ->
            case error of
                Http.BadStatus response ->
                    updateTodaysTasks todaysTasks { todaysTask | errors = decodeError response.body } ! []

                _ ->
                    todaysTasks ! []

        SendDestroyRequest todaysTaskId ->
            todaysTasks ! [ todaysTaskRequest (Destroy todaysTaskId) ]

        SendCompleteRequest todaysTaskId ->
            todaysTasks ! [ todaysTaskRequest (Complete todaysTaskId) ]

        SendUpdateRequest todaysTask ->
            updateTodaysTasks todaysTasks { todaysTask | status = Sending } ! [ todaysTaskRequest (Update todaysTask) ]

        DestroyTag todaysTask targetTagId (Ok _) ->
            let
                updatedTags =
                    List.filter (\tag -> tag.id /= targetTagId) todaysTask.tags

                updatedTodaysTask =
                    { todaysTask | tags = updatedTags }
            in
                updateTodaysTasks todaysTasks updatedTodaysTask ! []

        CreateTag todaysTask (Ok tag) ->
            let
                updatedTags =
                    todaysTask.tags ++ [ tag ]

                updatedTodaysTask =
                    { todaysTask | tags = updatedTags, tagInput = "", tagErrors = [] }
            in
                updateTodaysTasks todaysTasks updatedTodaysTask ! []

        CreateTag todaysTask (Err error) ->
            case error of
                Http.BadStatus response ->
                    updateTodaysTasks todaysTasks { todaysTask | tagErrors = decodeError response.body } ! []

                _ ->
                    todaysTasks ! []

        SendDestroyTagRequest todaysTask tagId ->
            todaysTasks ! [ destroyTagRequest todaysTask tagId ]

        SendCreateTagRequest todaysTask tagName ->
            todaysTasks ! [ createTagRequest todaysTask tagName ]

        SetTaskDoing todaysTaskId ->
            { todaysTasks | doingTaskId = Just todaysTaskId } ! []

        _ ->
            todaysTasks ! []


removeTodaysTask : List TodaysTask -> TodaysTaskId -> List TodaysTask
removeTodaysTask todaysTasks targetId =
    List.filter (\todays_task -> todays_task.id /= targetId) todaysTasks


updateTodaysTasks : TodaysTasks -> TodaysTask -> TodaysTasks
updateTodaysTasks todaysTasks updatedTodaysTask =
    let
        updateTodaysTask currentTodaysTask =
            if updatedTodaysTask.id == currentTodaysTask.id then
                updatedTodaysTask
            else
                currentTodaysTask
    in
        { todaysTasks | todaysTasks = List.map updateTodaysTask todaysTasks.todaysTasks }
