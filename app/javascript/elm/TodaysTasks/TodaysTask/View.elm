module TodaysTasks.TodaysTask.View exposing (todaysTaskView)

import Html exposing (Html, div, p, span, button, text, label, input, select, option)
import Html.Attributes exposing (class, classList, value, id, type_, attribute, disabled, selected)
import Html.Events exposing (onDoubleClick, onClick, onInput)
import TodaysTasks.TodaysTask.Model exposing (TodaysTask, TodaysTaskId, InputStatus(..), Priority)
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import TodaysTasks.Tag.View exposing (tagList, tagView, tagCreateForm)
import Helper exposing (onEnter, errorsView)


todaysTaskView : TodaysTaskId -> TodaysTask -> Html TodaysTasksMsg
todaysTaskView doingTaskId todaysTask =
    div [ class "card m-r-xs m-l-xs m-b-sm", onDoubleClick (EditingEntry todaysTask) ]
        [ div [ class "card-content p-md" ]
            [ div []
                [ p [ class "card-text" ]
                    [ text todaysTask.title ]
                , div [ class "tags has-addons m-b-none" ]
                    [ span [ class "tag is-dark" ]
                        [ text "priority" ]
                    , span [ class "tag is-info" ]
                        [ text (priorityToString todaysTask.priority) ]
                    ]
                , div [ class "tags m-b-none" ]
                    [ tagView todaysTask.tags (todaysTask.id == doingTaskId) ]
                , editButtons todaysTask doingTaskId
                ]
            ]
        , editForm todaysTask
        ]


editButtons : TodaysTask -> TodaysTaskId -> Html TodaysTasksMsg
editButtons todaysTask doingTaskId =
    div [ class "buttons" ]
        [ button [ class "button is-warning is-small", onClick (EditingEntry todaysTask) ]
            [ text "編集" ]
        , button [ class "button is-primary is-small", onClick (SendCompleteRequest todaysTask.id) ]
            [ text "完了" ]
        , if todaysTask.id == doingTaskId then
            text ""
          else
            button [ class "button is-link is-small", onClick (SetTaskDoing todaysTask.id) ]
                [ text "進行中にする" ]
        , button [ class "button is-danger is-small", onClick (SendDestroyRequest todaysTask.id) ]
            [ text "削除" ]
        ]


editForm : TodaysTask -> Html TodaysTasksMsg
editForm todaysTask =
    div [ classList [ ( "modal", True ), ( "is-active", todaysTask.status /= None ) ] ]
        [ div [ class "modal-background", onClick (UnEditingEntry todaysTask) ]
            []
        , div [ class "modal-content animated fadeInUp a-d-025" ]
            [ div [ class "card" ]
                [ div [ class "card-content" ]
                    [ label [ class "label" ]
                        [ text "タイトル" ]
                    , div [ class "field is-grouped" ]
                        [ p [ class "control is-expanded" ]
                            [ input
                                [ value todaysTask.input
                                , classList
                                    [ ( "input", True )
                                    , ( "is-danger", not (List.isEmpty todaysTask.errors) )
                                    ]
                                , id ("todaysTask-" ++ toString todaysTask.id)
                                , type_ "text"
                                , onEnter (SendUpdateRequest todaysTask)
                                , onInput (UpdateEditTextForm todaysTask)
                                ]
                                []
                            ]
                        , p [ class "control" ]
                            [ if todaysTask.status == Sending then
                                button [ class "button is-primary is-loading", disabled True ]
                                    [ text "更新" ]
                              else
                                button [ class "button is-primary", onClick (SendUpdateRequest todaysTask) ]
                                    [ text "更新" ]
                            ]
                        ]
                    , div [ class "errors" ]
                        (errorsView todaysTask.errors)
                    , priorityEditForm todaysTask
                    , tagList todaysTask
                    , tagCreateForm todaysTask
                    , div [ class "field is-grouped" ]
                        [ div [ class "control" ]
                            [ button [ class "button is-text", onClick (UnEditingEntry todaysTask) ]
                                [ text "閉じる" ]
                            ]
                        ]
                    ]
                ]
            ]
        , button [ attribute "aria-label" "close", class "modal-close is-large", onClick (UnEditingEntry todaysTask) ]
            []
        ]


priorityToString : Priority -> String
priorityToString priority =
    case priority of
        1 ->
            "最低"

        2 ->
            "低"

        3 ->
            "中"

        4 ->
            "高"

        5 ->
            "最高"

        _ ->
            ""


priorityEditForm : TodaysTask -> Html TodaysTasksMsg
priorityEditForm todaysTask =
    let
        prioritySelect priority =
            if priority == todaysTask.priority then
                option [ selected True ]
                    [ text (priorityToString priority) ]
            else
                option [ onClick (SendUpdateRequest { todaysTask | priority = priority }) ]
                    [ text (priorityToString priority) ]
    in
        div [ class "field" ]
            [ label [ class "label" ]
                [ text "優先度" ]
            , div [ class "control" ]
                [ div [ class "select" ]
                    [ select []
                        (List.map prioritySelect [ 1, 2, 3, 4, 5 ])
                    ]
                ]
            ]
