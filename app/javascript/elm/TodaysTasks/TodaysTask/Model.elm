module TodaysTasks.TodaysTask.Model exposing (TodaysTask, newTodaysTask, TaskTitle, TodaysTaskFlag, TodaysTaskId, Priority, InputStatus(..))

import TodaysTasks.Tag.Model exposing (Tag, TagName)


type alias TodaysTaskId =
    Int


type alias TaskTitle =
    String


type alias Priority =
    Int


type InputStatus
    = None
    | Editing
    | Sending


type alias TodaysTask =
    { id : TodaysTaskId
    , title : TaskTitle
    , priority : Priority
    , status : InputStatus
    , input : TaskTitle
    , errors : List String
    , tags : List Tag
    , tagErrors : List String
    , tagInput : TagName
    }


type alias TodaysTaskFlag =
    { id : TodaysTaskId
    , title : TaskTitle
    , priority : Priority
    , todays_task_tags : List Tag
    }


newTodaysTask : TodaysTaskId -> TaskTitle -> Priority -> List Tag -> TodaysTask
newTodaysTask todaysTaskId title priority tags =
    { id = todaysTaskId
    , title = title
    , priority = priority
    , status = None
    , input = title
    , errors = []
    , tags = tags
    , tagErrors = []
    , tagInput = ""
    }
