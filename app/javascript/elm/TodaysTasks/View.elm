module TodaysTasks.View exposing (view)

import Html exposing (Html, div, header, text, input, button, p)
import Html.Attributes exposing (class, classList, type_, value, disabled)
import Html.Events exposing (onInput, onClick)
import TodaysTasks.Model exposing (TodaysTasks)
import TodaysTasks.TodaysTask.View exposing (todaysTaskView)
import TodaysTasks.TodaysTask.Model exposing (TaskTitle, InputStatus(..))
import TodaysTasks.Msg exposing (TodaysTasksMsg(..))
import Msg exposing (Msg)
import Helper exposing (onEnter)


view : TodaysTasks -> Html Msg
view todaysTasks =
    Html.map Msg.TodaysTasksMsg (todaysTasksView todaysTasks)


todaysTasksView : TodaysTasks -> Html TodaysTasksMsg
todaysTasksView model =
    let
        doingTaskId =
            Maybe.withDefault 0 model.doingTaskId
    in
        div [ class "card" ]
            [ header [ class "card-header-title" ]
                [ text "タスク一覧" ]
            , div [ class "card-content p-xs" ]
                (List.map (todaysTaskView doingTaskId) model.todaysTasks ++ [ form model.input model.errors model.inputStatus ])
            ]


form : TaskTitle -> List String -> InputStatus -> Html TodaysTasksMsg
form newTodo errors inputStatus =
    div [ class "card m-r-xs m-l-xs m-b-xs" ]
        [ div [ class "card-content" ]
            [ div [ class "field" ]
                ([ input
                    [ type_ "text"
                    , value newTodo
                    , onInput UpdateNewTextForm
                    , onEnter SendCreateRequest
                    , classList
                        [ ( "input", True )
                        , ( "is-danger", not (List.isEmpty errors) )
                        ]
                    ]
                    []
                 ]
                    ++ List.map errorView errors
                )
            , if inputStatus == Sending then
                button [ class "button is-primary is-loading", disabled True ]
                    [ text "追加" ]
              else
                button [ class "button is-primary", onClick SendCreateRequest ]
                    [ text "追加" ]
            ]
        ]


errorView : String -> Html TodaysTasksMsg
errorView error =
    p [ class "help is-danger" ]
        [ text error ]
