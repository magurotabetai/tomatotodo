module TodaysTasks.Model exposing (TodaysTasks, initTodaysTasks, TodaysTasksFlag)

import TodaysTasks.TodaysTask.Model exposing (TodaysTask, TaskTitle, TodaysTaskId, TodaysTaskFlag, newTodaysTask, InputStatus(..))


type alias TodaysTasks =
    { todaysTasks : List TodaysTask
    , input : TaskTitle
    , errors : List String
    , doingTaskId : Maybe TodaysTaskId
    , inputStatus : InputStatus
    }


type alias TodaysTasksFlag =
    List TodaysTaskFlag


initTodaysTasks : TodaysTasksFlag -> TodaysTasks
initTodaysTasks flags =
    let
        flagToTodaysTask flag =
            newTodaysTask flag.id flag.title flag.priority flag.todays_task_tags

        todaysTasks =
            List.map flagToTodaysTask flags
    in
        TodaysTasks todaysTasks "" [] (Maybe.map .id (List.head todaysTasks)) None
