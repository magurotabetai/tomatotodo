# == Schema Information
#
# Table name: big_tasks
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  user_id    :integer          not null
#  status     :integer          default("backlog"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_big_tasks_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :big_task do
    sequence(:title) { |n| "アクティビティ在庫#{n}" }
    status 1
  end
end
