# == Schema Information
#
# Table name: todays_task_tags
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_todays_task_tags_on_name_and_user_id  (name,user_id) UNIQUE
#  index_todays_task_tags_on_user_id           (user_id)
#

FactoryBot.define do
  factory :todays_task_tag do
    name 'MyString'
  end
end
