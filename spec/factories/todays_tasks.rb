# == Schema Information
#
# Table name: todays_tasks
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          not null
#  expected_pomodoro_count :integer
#  pomodoro_count          :integer
#  status                  :integer          default("backlog"), not null
#  priority                :integer          default(1)
#  title                   :string           not null
#  memo                    :text             default(""), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_todays_tasks_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :todays_task do
    expected_pomodoro_count { rand(1..7) }
    pomodoro_count { rand(1..7) }
    status { :backlog }
    priority { rand(1..5) }
    sequence(:memo) { |n| "メモ#{n}" }
    sequence(:title) { |n| "タイトル#{n}" }
  end
end
