# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  username           :string           default(""), not null
#  encrypted_password :string           default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :inet
#  last_sign_in_ip    :inet
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "user#{n}" }
    name { username }
    password 'hogehoge'
  end
end
