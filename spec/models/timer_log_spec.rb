# == Schema Information
#
# Table name: timer_logs
#
#  id             :integer          not null, primary key
#  started_at     :datetime         not null
#  ended_at       :datetime         not null
#  status         :integer          default("done"), not null
#  user_id        :integer          not null
#  todays_task_id :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_timer_logs_on_todays_task_id  (todays_task_id)
#  index_timer_logs_on_user_id         (user_id)
#

require 'rails_helper'

RSpec.describe TimerLog, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
