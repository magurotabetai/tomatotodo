# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  username           :string           default(""), not null
#  encrypted_password :string           default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :inet
#  last_sign_in_ip    :inet
#  name               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '.from_omniauth' do
    context 'Twitterからサインインする場合' do
      let(:username) { 'tos' }
      let(:auth) do
        {
          'info' => { 'nickname' => username },
          'uid'  => '0000000000000000000000000',
          'provider' => 'twitter'
        }
      end

      context 'usernameがすでに登録されていた場合' do
        before do
          create(:user, username: username)
        end

        example '別のusernameのアカウントが作成される' do
          expect { User.from_omniauth(auth) }.to change { User.count }.by(+1)
          expect(User.last.username).not_to eq username
        end
      end

      context 'usernameが登録されていなかった場合' do
        example 'usernameのアカウントが作成される' do
          expect { User.from_omniauth(auth) }.to change { User.count }.by(+1)
          expect(User.last.username).to eq username
        end
      end
    end
  end
end
