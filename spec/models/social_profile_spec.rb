# == Schema Information
#
# Table name: social_profiles
#
#  id         :integer          not null, primary key
#  provider   :integer          not null
#  uid        :string           not null
#  username   :string           default(""), not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_social_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe SocialProfile, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
