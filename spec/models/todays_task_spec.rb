# == Schema Information
#
# Table name: todays_tasks
#
#  id                      :integer          not null, primary key
#  user_id                 :integer          not null
#  expected_pomodoro_count :integer
#  pomodoro_count          :integer
#  status                  :integer          default("backlog"), not null
#  priority                :integer          default(1)
#  title                   :string           not null
#  memo                    :text             default(""), not null
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_todays_tasks_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe TodaysTask, type: :model do
  let(:user) { create(:user) }
  let(:todays_task) { create(:todays_task, user: user) }

  describe '#add_tag' do
    let(:tag_name) { 'feature' }
    subject(:add_tag) { todays_task.add_tag(tag_name) }

    context 'すでに同名のタグが登録されている場合' do
      let!(:exists_tag) { create(:todays_task_tag, name: tag_name, user: user) }

      it { is_expected.to eq exists_tag }
      it { expect { add_tag }.not_to change { TodaysTaskTag.count } }
      it { expect { add_tag }.to change { todays_task.todays_task_tags.count }.from(0).to(1) }
    end

    context '同名のタグが登録されていなかった場合' do
      it { expect(add_tag.name).to eq tag_name }
      it { expect { add_tag }.to change { todays_task.todays_task_tags.count }.from(0).to(1) }
    end
  end

  describe '#remove_tag' do
    let(:tag_name) { 'feature' }
    subject(:remove_tag) { todays_task.remove_tag(tag_name) }

    context 'タグが存在する場合' do
      before { todays_task.add_tag(tag_name) }

      it { expect(remove_tag.name).to eq tag_name }
      it { expect { remove_tag }.not_to change { TodaysTaskTag.count } }
      it { expect { remove_tag }.to change { todays_task.todays_task_tags.count }.from(1).to(0) }
    end

    context 'タグが存在していない場合' do
      it { expect { remove_tag }.to raise_error(ActiveRecord::RecordNotFound) }
    end
  end
end
